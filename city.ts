import * as _ from "lodash";
import { iAddress } from "./interface-address";

export default class City implements iAddress {
    private _city: string;

    /**
     * Setter methods - Set city
     * @param {string} temp 
     * @returns {boolean} return true if meet conditions otherwise return false
     */
    set(temp: string): boolean {
        const arrCity = ["Kuala Terengganu","Kuala Lumpur","Kajang","Bangi","Damansara","Petaling Jaya","Puchong","Subang Jaya","Cyberjaya","Putrajaya","Mantin","Kuching","Seremban"]
        if(_.includes(arrCity, temp)){
            this._city = temp;
            return true;
        }
        return false;
    }

    /**
     * Getter methods - Get city
     * @returns {string} city
     */
    get(): string {
        return this._city;
    }
}