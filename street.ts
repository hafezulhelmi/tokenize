import { iAddress } from "./interface-address";

export default class Street implements iAddress {
    private _street: string;

    /**
     * Setter methods - Set street
     * @param {string} temp 
     * @returns {boolean} return true
     */
    set(temp: string): boolean {
        const regex = "(Jalan|Jln|Lorong|Persiaran)+\\s+(\\w+)";
        if(temp.match(regex)){
            this._street = temp;
            return true;
        }
        return false;
    }

    /**
     * Getter methods - Get street
     * @returns {string} street
     */
    get(): string {
        return this._street;
    }
}