import * as _ from "lodash";
import { expect } from "chai";
import Address from "./address";

// (Command)
// npm run test-filename ./-- -g ".\address.test.ts"

describe("Tokenize Address", () => {
    it("No 11, Chendering, 21080 Kuala Terengganu, Terengganu", async () => {
        const address = new Address();
        const res = address.tokenize("No 11, Chendering, 21080 Kuala Terengganu, Terengganu");
        expect(res).to.deep.equal({
            apt: 'No 11',
            street: undefined,
            section: 'Chendering',
            postcode: '21080',
            city: 'Kuala Terengganu',
            state: 'Terengganu'
          });
    });

    it("No 11, Kuala Terengganu, Chendering", async () => {
        const address = new Address();
        const res = address.tokenize("No 11, Kuala Terengganu, Chendering");
        expect(res).to.deep.equal({
            apt: 'No 11',
            street: undefined,
            section: 'Chendering',
            postcode: undefined,
            city: 'Kuala Terengganu',
            state: undefined
          });
    });

    it("No 20", async () => {
        const address = new Address();
        const res = address.tokenize("No 20");
        expect(res).to.deep.equal({
            apt: 'No 20',
            street: undefined,
            section: undefined,
            postcode: undefined,
            city: undefined,
            state: undefined
          });
    });

    it("No 20, Puchong", async () => {
        const address = new Address();
        const res = address.tokenize("No 20, Puchong");
        expect(res).to.deep.equal({
            apt: 'No 20',
            street: undefined,
            section: undefined,
            postcode: undefined,
            city: 'Puchong',
            state: undefined, 
          });
    });

    it("No 20, Puchong, Selangor", async () => {
        const address = new Address();
        const res = address.tokenize("No 20, Puchong, Selangor");
        expect(res).to.deep.equal({
            apt: 'No 20',
            street: undefined,
            section: undefined,
            postcode: undefined,
            city: 'Puchong',
            state: 'Selangor' 
          });
    });

    it("No 20, 47100 Puchong, Selangor", async () => {
        const address = new Address();
        const res = address.tokenize("No 20, 47100 Puchong, Selangor");
        expect(res).to.deep.equal({
            apt: 'No 20',
            street: undefined,
            section: undefined,
            postcode: '47100',
            city: 'Puchong',
            state: 'Selangor' 
          });
    });

    it("No 20, Persiaran Kinrara, 47100 Puchong, Selangor", async () => {
        const address = new Address();
        const res = address.tokenize("No 20, Persiaran Kinrara, 47100 Puchong, Selangor");
        expect(res).to.deep.equal({
            apt: 'No 20',
            street: 'Persiaran Kinrara',
            section: undefined,
            postcode: '47100',
            city: 'Puchong',
            state: 'Selangor' 
          });
    });

    it("No 20, Persiaran Kinrara, Taman Kinrara Seksyen 3, 47100 Puchong, Selangor", async () => {
        const address = new Address();
        const res = address.tokenize("No 20, Persiaran Kinrara, Taman Kinrara Seksyen 3, 47100 Puchong, Selangor");
        expect(res).to.deep.equal({
            apt: 'No 20',
            street: 'Persiaran Kinrara',
            section: 'Taman Kinrara Seksyen 3',
            postcode: '47100',
            city: 'Puchong',
            state: 'Selangor' 
          });
    });

    it("01000", async () => {
        const address = new Address();
        const res = address.tokenize("01000");
        expect(res).to.deep.equal({
            apt: undefined,
            street: undefined,
            section: undefined,
            postcode: "01000",
            city: undefined,
            state: undefined
          });
    });

    it("Jln Setia Alam", async () => {
        const address = new Address();
        const res = address.tokenize("Jln Setia Alam");
        expect(res).to.deep.equal({
            apt: undefined,
            street: "Jln Setia Alam",
            section: undefined,
            postcode: undefined,
            city: undefined,
            state: undefined
          });
    });
});
