export interface iAddress{
    set(temp: string): boolean;
    get(): string;
}

export interface iPostcode{
    isPostcode(item: string): boolean;
}