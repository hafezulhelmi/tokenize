import Apt from "./apt";
import City from "./city";
import State from "./state";
import Postcode from "./postcode";
import Section from "./section";
import Street from "./street";
import * as _ from "lodash";

interface iTokenize {
    apt?: string,
    section?: string,
    postcode?: string,
    city?: string,
    state?: string,
    street?: string,
}

export default class Address {
    protected apt: Apt;
    protected city: City;
    protected state: State;
    protected street: Street;
    protected postcode: Postcode;
    protected section: Section;

    constructor() {
        this.apt = new Apt();
        this.city = new City();
        this.state = new State();
        this.street = new Street();
        this.section = new Section();
        this.postcode = new Postcode();
    }

    /**
     * To add coma between postcode and city
     * @param {string} address full address
     * @returns {string} new address
     */
    private _addComaPostcodeCity(address: string): string {
        const addressArr = address.split(/(?:,| )+/);
        for (let temp of addressArr) {
            temp = temp.trim();
            if(this.postcode.isPostcode(temp)){
                address = address.replace(temp, temp + ",");  
            }
        }
        return address;
    }

    /**
     * Set address components
     * @param {string} full address
     * @returns {void} 
     */
    private _setAddressComponents(address: string): void {
        address = this._addComaPostcodeCity(address);
        const addressArr = _.compact(address.split(","));
        for (let temp of addressArr) {
            temp = temp.trim();
            if (!(this.apt.set(temp) || this.postcode.set(temp) || this.city.set(temp) || this.state.set(temp) || this.street.set(temp))) {
                this.section.set(temp);
            }
        }
    }

    /**
     * Get address components
     * @returns {iTokenize}
     */
    private _getAddressComponents(): iTokenize {
        return {
            apt: this.apt.get(),
            street: this.street.get(),
            section: this.section.get(),
            postcode: this.postcode.get(),
            city: this.city.get(),
            state: this.state.get(),
        }
    }

    /**
     * Tokenize a free form text into address components
     * @param {string} address full address
     * @returns {iTokenize}
     */
    tokenize(address: string): iTokenize {
        this._setAddressComponents(address);
        return this._getAddressComponents();
    }
}