import { iAddress } from "./interface-address";

export default class Apt implements iAddress {
    private _apt: string;

    /**
     * Setter methods - Set apt
     * @param {string} temp 
     * @returns {boolean} return true if meet conditions otherwise return false
     */
    set(temp: string): boolean {
        const regex = "(No)+\\s+(\\d+)";
        if(temp.match(regex)){
            this._apt = temp;
            return true;
        }
        return false;
    }

    /**
     * Getter methods - Get apt
     * @returns {string} apt
     */
    get(): string {
        return this._apt;
    }
}