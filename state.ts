import * as _ from "lodash";
import { iAddress } from "./interface-address";

export default class State implements iAddress {
    private _state: string;

    /**
     * Setter methods - Set state
     * @param {string} temp 
     * @returns {boolean} return true if meet conditions otherwise return false
     */
    set(temp: string): boolean {
        const arrState = ["Selangor","Terengganu","Pahang","Kelantan","Melaka","Pulau Pinang","Kedah","Johor","Perlis","Sabah","Sarawak"]
        if(_.includes(arrState, temp)){
            this._state = temp;
            return true;
        }
        return false;
    }

    /**
     * Getter methods - Get state
     * @returns {string} state
     */
    get(): string {
        return this._state;
    }
}