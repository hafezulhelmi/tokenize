import { iAddress } from "./interface-address";

export default class Section implements iAddress  {
    private _section: string;

    /**
     * Setter methods - Set section
     * @param {string} temp 
     * @returns {boolean} return true
     */
    set(temp: string): boolean {
        this._section = (this._section) ? `${this._section}, ${temp}` : temp;
        return true;
    }

    /**
     * Getter methods - Get section
     * @returns {string} section
     */
    get(): string {
        return this._section;
    }
}