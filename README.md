# Tokenize

Tokenize a free form text into address components.

Can handle comma only.

## Installation typesccript and ts-node
```bash
npm install typescript ts-node
```

## Run Unit Test
```bash
npm run test-filename ./-- -g ".\address.test.ts"
```