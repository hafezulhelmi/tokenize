import { iAddress, iPostcode } from "./interface-address";

export default class Postcode implements iAddress, iPostcode {
  private _postcode: string;

  /**
   * Check numbers from range of 01000-98859
   * @param {string} item 
   * @returns return true if meet conditions otherwise return false
   */
  isPostcode(item: string): boolean {
    return (Number(item) >= 1000 && Number(item) <= 98859);
  }

  /**
   * Setter methods - Set apt
   * @param {string} temp 
   * @returns {boolean} return true if meet conditions otherwise return false
   */
  set(temp: string): boolean {
    const regex = "(\\d{5})";
    if(temp.match(regex) && this.isPostcode(temp)){
      this._postcode = temp;
      return true;
    }
    return false;
  }

  /**
   * Getter methods - Get postcode
   * @returns {string} postcode
   */
  get(): string {
      return this._postcode;
  }
}